import React from 'react';
import { StyleSheet, ScrollView, AsyncStorage } from 'react-native';
import {
    Container,
    StyleProvider,
    View,
    Button,
    Text,
    Icon,
    Thumbnail
} from 'native-base';

import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import { newList, newItem } from '../utils/ListUtils';
import ToggleableListForm from '../components/ToggleableListForm';
import ListTemp from "../components/ListTemp";

const firebase = require("firebase");

const firebaseConfig = {
    apiKey: "AIzaSyB8FUzZXH3ueEKhfuhw8gt57r9zu9a0L1U",
    authDomain: "lists-226217.firebaseapp.com",
    databaseURL: "https://lists-226217.firebaseio.com",
    projectId: "lists-226217",
    storageBucket: "lists-226217.appspot.com",
    messagingSenderId: "806714238652"
};

firebase.initializeApp(firebaseConfig);

export default class MainScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            lists: [],
            user: {},
            photoUrl: 'https://www.das.edu/wp-content/uploads/userphoto.png',
            username: '',
            data: [],
            email: '',
            publicLists: [],
            firebaseLists: [],
        };
    }

    componentDidMount() {
        //this.setState({lists: []})
        this.getLocal('lists');
        const that = this;
        firebase
            .auth()
            .onAuthStateChanged(function (user) {
                if (user) {
                    // User is signed in.
                    console.log('This is user on MainScreen ', user.providerData[0]);
                    console.log('This is userID on MainScreen ', user.providerData[0].uid);

                    const photoUrl = user.providerData[0].photoURL ? user.providerData[0].photoURL : that.state.photoUrl ;
                    that.setState({ user: user, photoUrl: photoUrl, username: user.providerData[0].displayName, email: user.providerData[0].email })

                } else {
                    // User is signed out.
                }
            });
    }

    saveLocal = ((key, data) => {
        AsyncStorage.setItem(key, JSON.stringify(data), () => {
            this.setState({ lists: data })
        });

    })

    getLocal = ((key) => {
        AsyncStorage.getItem(key, (err, result) => {
            if (result !== 'undefined' && result !== null) {
                this.setState({
                    lists: JSON
                        .parse(result)
                        .map(list => {
                            if (list.id) {
                                return ({
                                    ...list,
                                    isExpand: false,
                                    buttonGroupOpen: false,
                                })
                            }
                        })
                })
            } else if (result === 'undefined' || result === null) {
                this.setState({ lists: [] })
            }
        });
    })

    writeUserData(key, name, email, imageUrl) {
        firebase.database().ref('users/').child(key).set({
            username: name,
            email: email,
            profile_picture: imageUrl
        });
    }

    handleCreateFormSubmit = list => {
        const { lists, email } = this.state;

        this.saveLocal('lists', [
            newList(list, email), ...lists
        ])
    };

    handleItemCreate = (title, listId) => {
        const { lists } = this.state;

        const middleValue = lists.map(list => {
            if (list.id === listId) {
                return {
                    ...list,
                    positions: [
                        ...list.positions,
                        newItem(title)
                    ]
                }
            }
            return list;
        })
        this.saveLocal('lists', middleValue)
    };

    handleItemRemove = (listId, itemId) => {
        const { lists } = this.state;

        const middleValue = lists.map((list, id) => {
            if (list.id === listId) {
                return {
                    ...list,
                    positions: list
                        .positions
                        .filter(i => i.id !== itemId)
                }
            }
            return list;
        })
        this.saveLocal('lists', middleValue)
    }

    handleFormSubmit = attrs => {
        const { lists } = this.state;

        const middleValue = lists.map(list => {
            if (list.id === attrs.id) {
                const { title } = attrs;

                return {
                    ...list,
                    title
                };
            }

            return list;
        })
        this.saveLocal('lists', middleValue)

    };

    handleRemovePress = listId => {
        const middleValue = this
            .state
            .lists
            .filter(t => t.id !== listId)
        this.saveLocal('lists', middleValue)
    };

    toggleExpand = listId => {
        const { lists } = this.state;
        console.log('THIS STATE LISTS ', this.state.lists)
        const middleValue = lists.map(list => {
            const { id, isExpand } = list;

            if (id === listId) {
                return {
                    ...list,
                    isExpand: !isExpand
                };
            }

            return list;
        })
        this.saveLocal('lists', middleValue)

    };

    toggleDialog = (listId) => {
        const { lists } = this.state;

        const middleValue = lists.map(list => {
            const { id, isDialogOpen } = list;

            if (id === listId) {
                return {
                    ...list,
                    isDialogOpen: !isDialogOpen
                };
            }
            return list;
        })
        this.saveLocal('lists', middleValue)
    };

    handleOpenButtonGroup = (listId) => {
        const { lists } = this.state;
        const middleValue = lists.map(list => {
            const { id, buttonGroupOpen } = list;

            if (id === listId) {
                return {
                    ...list,
                    buttonGroupOpen: !buttonGroupOpen
                };
            }
            return list;
        })
        this.saveLocal('lists', middleValue)
    }


    signOut = async () => {
        try {
            firebase
                .auth()
                .signOut();
            this
                .props
                .navigation
                .navigate('Auth')
        } catch (error) {
            alert(error)
        }
    }

    render() {
        const { lists } = this.state;

        return (
            <StyleProvider style={getTheme(material)}>
                <Container style={styles.mainContainer}>
                    <View style={styles.backBlock1} />
                    <View style={styles.backBlock2} />
                    <ScrollView contentContainerStyle={styles.scrollView}>
                        <View style={styles.backBlock3} />
                        <View style={styles.userbar}>
                            <Thumbnail small source={{ uri: this.state.photoUrl }} />
                            <Text style={styles.userbarText}>
                                {this.state.username}
                            </Text>
                            <Button iconLeft transparent dark onPress={this.signOut}>
                                <Icon name='log-out' />
                                <Text>Log out</Text>
                            </Button>
                        </View>
                        <View style={styles.toggleableContainer}>
                            <ToggleableListForm onFormSubmit={this.handleCreateFormSubmit} />
                        </View>
                        {/*<View>
                            <ListTemp
                                data={lists}
                                onFormSubmit={this.handleFormSubmit}
                                onItemCreate={this.handleItemCreate}
                                onItemRemove={this.handleItemRemove}
                                onRemovePress={this.handleRemovePress}
                                onStartPress={this.toggleList}
                                onStopPress={this.toggleList}
                                onExpandPress={this.toggleExpand}
                                onMinimizePress={this.toggleExpand}
                                onDialogToggle={this.toggleDialog}
                                onPressOpenButtonGroup={this.handleOpenButtonGroup} />
                        </View>*/}
                    </ScrollView>
                </Container>
            </StyleProvider>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: material.bgColor,
    },
    backBlock1: {
        backgroundColor: material.headColor,
        position: 'absolute',
        left: -200,
        top: 300,
        width: 1000,
        height: 200,
        transform: [{ rotate: '31 deg' }]
    },
    backBlock2: {
        backgroundColor: material.brandPrimary,
        position: 'absolute',
        left: -200,
        top: 400,
        width: 1000,
        height: 200,
        transform: [{ rotate: '310 deg' }]
    },
    userbar: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        //borderRightWidth: 10,
        //borderLeftWidth: 10,
        //borderBottomWidth: 10,
        //borderTopWidth: 10,
        //borderColor: material.brColor,
        padding: 10,
        alignItems: 'center',
        //borderBottomLeftRadius: 10,
        //borderBottomRightRadius: 10,
        backgroundColor: material.headColor,

    },
    userbarText: {
        color: material.ubContentColor
    },
    scrollView: {
        padding: 4,
        flexDirection: 'column',
        paddingBottom: 40
    },
    toggleableContainer: {
        margin: 5,
    }
})
