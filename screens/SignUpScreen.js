import React from 'react';
import { StyleSheet, Text, StatusBar } from 'react-native';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
const firebase = require("firebase");

import {
    Container,
    StyleProvider,
    View,
    Header,
    Form,
    Input,
    Item,
    Button,
    Label,
    Left,
    Right, Body, Icon, Title

} from 'native-base';

export default class SignUpScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = ({ email: '', password: '', signUpError: '' })
        
    }

    signUpUser = (email, password) => {
        const that = this

        try {
            if (this.state.password.length < 2) {
                alert("Please enter at least 2 characters");
                return;
            }
            firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
                if (error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    that.setState({ signUpError: errorMessage, email: '', password: '' })

                }
                else {
                    that.setState({ email: '', password: '' });
                    that.props.navigation.navigate('SignIn');
                }


            });
        }
        catch (error) {

        }


    }

    backToLogin() {
        this.setState({ email: '', password: '', anError: '' })
        this.props.navigation.navigate('SignIn')
    }

    render() {
        const { email, password } = this.state;
        return (
            <StyleProvider style={getTheme(material)}>
            <Container style={styles.mainContainer}>
                <View style={styles.warning}>
                    <Text style={styles.signUpError}>
                        {this.state.signUpError}
                    </Text>
                </View>
                <Form>
                    <View style={styles.input}>

                        <Item stackedLabel>
                            <Label>Email</Label>
                            <Input
                                value={this.state.email}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(email) => this.setState({ email })} />
                        </Item>
                    </View>
                    <View style={styles.input}>
                        <Item stackedLabel>
                            <Label>Password</Label>
                            <Input
                                value={this.state.password}
                                secureTextEntry={true}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(password) => this.setState({ password })} />
                        </Item>
                    </View>

                    <View style={styles.buttonGroup}>
                        <View style={styles.button}>
                            <Button
                                style={styles.authButton}
                                block
                                success
                                onPress={() => this.signUpUser(email, password)}>

                                <Text>Sign up</Text>
                            </Button>
                        </View>
                        <View style={styles.button}>
                            <Button
                                style={styles.authButton}
                                block
                                success
                                transparent
                                onPress={() => this.backToLogin()}
                            >
                                <Text>Back to login</Text>
                            </Button>
                        </View>

                    </View>


                </Form>
            </Container>
            </StyleProvider>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
    },
    signUpError: {
        fontSize: 18,
        alignContent: 'center',
        color: 'red',
        marginBottom: 15
    },
    warningText: {
        fontSize: 18,
    },
    input: {
        marginBottom: 15
    },
    buttonGroup: {
        flexDirection: 'column',
    },
    button: {
        margin: 5
    }
})
