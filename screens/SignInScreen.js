import React from 'react';
import { StyleSheet } from 'react-native';
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin'
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';

const firebase = require("firebase");

import {
    Container,
    StyleProvider,
    View,
    Form,
    Input,
    Item,
    Button,
    Label,
    Text,
    Icon,
} from 'native-base';

export default class SignInScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = ({ email: '', password: '', loginError: '' })
    }

    componentDidMount() {
        this.setupGoogleSignin();
    }

    loginUser = (email, password) => {

        const that = this

        firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            that.setState({ loginError: errorMessage })
        });

        firebase.auth().currentUser ? this.props.navigation.navigate('AuthLoading') : () => { }

    }

    googleAuth() {
        GoogleSignin
            .signIn()
            .then((user) => {
                const credential = firebase.auth.GoogleAuthProvider.credential(user.idToken, user.accessToken);
                return firebase.auth().signInWithCredential(credential);
            })
            .then((currentUser) => {
                console.log(`Google Login with user: ${JSON.stringify(currentUser.toJSON())}`);
            })
            .catch((err) => {
                console.log('WRONG SIGNIN', err);
            })
            .done();
    }

    //СДЕЛАТЬ ДЛЯ IOS!

    async setupGoogleSignin() {
        try {
            await GoogleSignin.configure({
                webClientId: '806714238652-rbvnd87krf6b5r0r1ip9j50mc3vkc1p1.apps.googleusercontent.com',
                offlineAccess: false
            });

            const user = await GoogleSignin.currentUserAsync();
            console.log(user);
        }
        catch (err) {
            console.log("Google signin error", err.code, err.message);
        }
    }

    render() {
        const { email, password } = this.state;
        return (
            <StyleProvider style={getTheme(material)}>
                <Container style={styles.mainContainer}>
                    <View>
                        <Text style={styles.loginError}>
                            {this.state.loginError}
                        </Text>
                    </View>
                    <View style={styles.googleButton}>
                        <Button
                            iconleft
                            transparent
                            onPress={this.googleAuth}
                            >
                            <Icon type='Ionicons' fontSize={50} name='logo-google'/>
                            <Text>Sign in with google</Text>
                        </Button>
                    </View>
                    <Form>
                        <View style={styles.input}>
                            <Item stackedLabel>
                                <Label>Email</Label>
                                <Input
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    onChangeText={(email) => this.setState({ email })} />
                            </Item>
                        </View>
                        <View style={styles.input}>
                            <Item stackedLabel>
                                <Label>Password</Label>
                                <Input
                                    secureTextEntry={true}
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    onChangeText={(password) => this.setState({ password })} />
                            </Item>
                        </View>

                        <View style={styles.buttonGroup}>
                            <View style={styles.button}>
                                <Button
                                    style={styles.authButton}
                                    block
                                    success
                                    onPress={() => this.loginUser(email, password)}
                                >
                                    <Text style={styles.authButtonText}>Sign in</Text>
                                </Button>
                            </View>

                            <View style={styles.button}>
                                <Button
                                    style={styles.authButton}
                                    block
                                    transparent
                                    onPress={() => this.props.navigation.navigate('Register')}>

                                    <Text style={styles.authButtonText}>Sign up</Text>
                                </Button>
                            </View>

                        </View>

                    </Form>
                </Container>
            </StyleProvider>

        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: material.bgColor,
    },
    loginError: {
        fontSize: 18,
        alignContent: 'center',
        color: 'red',
        marginBottom: 15
    },
    input: {
        marginBottom: 15
    },
    buttonGroup: {
        flexDirection: 'row',
    },
    button: {
        flex: 1, margin: 5,
    },
    googleButton: {
        backgroundColor: 'white',
    }
})
