import React from 'react';
import { StyleSheet } from 'react-native';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';

const firebase = require("firebase");

import {
    Container,
    StyleProvider,
    View,
    Form,
    Input,
    Item,
    Button,
    Label,
    Text,
    Icon,
} from 'native-base';

export default class SignInScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = ({ displayName: '', tip: '' })
    }

    componentDidMount() {
    }

    changeUsername = () => {
        const that = this;
        var user = firebase.auth().currentUser;
        if(displayName.length > 2){
            user.updateProfile({
                displayName: that.state.displayName,
            }).then(function() {
                that.setState({ tip: 'Username did set successfuly'});
                that.props.navigation.navigate('AuthLoading');
            }).catch(function(error){
                that.setState({tip: error})
            })
        }
        else {
            this.setState({tip: "Username must be at least 3 characters"})
        }

    }


    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Container style={styles.mainContainer}>
                    <View>
                        <Text style={styles.loginError}>
                            {this.state.tip}
                        </Text>
                    </View>

                    <Form>
                        <View style={styles.input}>
                            <Item stackedLabel>
                                <Label>Type username you want</Label>
                                <Input
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    onChangeText={(displayName) => this.setState({ displayName })} />
                            </Item>
                        </View>

                        <View style={styles.buttonGroup}>
                            <View style={styles.button}>
                                <Button
                                    style={styles.authButton}
                                    block
                                    success
                                    onPress={() => this.changeUsername()}
                                >
                                    <Text style={styles.authButtonText}>Set username</Text>
                                </Button>
                            </View>

                        </View>

                    </Form>
                </Container>
            </StyleProvider>

        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: material.bgColor,
    },
    loginError: {
        fontSize: 18,
        alignContent: 'center',
        color: 'red',
        marginBottom: 15
    },
    input: {
        marginBottom: 15
    },
    buttonGroup: {
        flexDirection: 'row',
    },
    button: {
        flex: 1, margin: 5,
    },
    googleButton: {
        backgroundColor: 'white',
    }
})
