import React from 'react';
import {StyleSheet, View, Text, ActivityIndicator, AsyncStorage} from 'react-native';

const firebase = require("firebase");

export default class AuthLoadingScreen extends React.Component {
    constructor(props){
        super(props);
        this.loadApp()
    }

    loadApp = async() => {
        const that = this;
        firebase.auth().onAuthStateChanged(function(user) {
            user 
            ? user.displayName 
            ? that.props.navigation.navigate('App') 
            : that.props.navigation.navigate('SetUpName')
            : that.props.navigation.navigate('SignIn') 
        })

    }
    render() {

        return (
            <View style={styles.loading}>
                <ActivityIndicator/>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
