import { Root } from 'native-base';
import React from 'react';
import { createSwitchNavigator, createStackNavigator, createDrawerNavigator } from 'react-navigation';
import AuthLoadingScreen from './screens/AuthLoadingScreen';
import SignInScreen from './screens/SignInScreen';
import DisplayNameSetUp from './screens/DisplayNameSetUp';
import SignUpScreen from './screens/SignUpScreen';
import MainScreen from './screens/MainScreen';

const setDisplayNameStackNavigator = createDrawerNavigator({
    DisplayNameSetUp: DisplayNameSetUp,
})

const AuthStackNavigator = createDrawerNavigator({
    SignIn: SignInScreen,
})

const RegisterStackNavigator = createDrawerNavigator({
    SignUp: SignUpScreen,
})

const AppDrawerNavigator = createDrawerNavigator({
    Home: MainScreen
})
const AppNavigator = createSwitchNavigator({
    AuthLoading: AuthLoadingScreen,
    Auth: AuthStackNavigator,
    App: AppDrawerNavigator,
    Register: RegisterStackNavigator,
    SetUpName: setDisplayNameStackNavigator
})

export default () => 
    <Root>
        <AppNavigator/>
    </Root>
