import React from 'react';
import {
  StyleProvider,
  View,
} from 'native-base';
import { StyleSheet } from 'react-native';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';

import ListForm from './ListForm';
import List from './List';

export default class EditableList extends React.Component {

  state = {
    editFormOpen: false,
  };

  handleEditPress = () => {
    this.openForm();
  };

  handleFormClose = () => {
    this.closeForm();
  };

  handleSubmit = list => {
    const { onFormSubmit } = this.props;

    onFormSubmit(list);
    this.closeForm();
  };

  closeForm = () => {
    this.setState({ editFormOpen: false });
  };

  openForm = () => {
    this.setState({ editFormOpen: true });
  };

  render() {
    const {
      id,
      title,
      positions,
      isExpand,
      isDialogOpen,
      buttonGroupOpen,
      onItemPress,
      onRemovePress,
      onItemCreate,
      onExpandPress,
      onMinimizePress,
      onItemRemove,
      onDialogToggle,
      onPressOpenButtonGroup,
    } = this.props;
    const { editFormOpen } = this.state;

    if (editFormOpen) {
      return (
        <StyleProvider style={getTheme(material)}>
          <View style={styles.openedFormView}>
            <ListForm
              textInputLabel='Edit title'
              id={id}
              title={title}
              onFormSubmit={this.handleSubmit}
              onFormClose={this.handleFormClose}
            />
          </View>
        </StyleProvider>


      );
    }
    return (
      <List
        id={id}
        title={title}
        isExpand={isExpand}
        positions={positions}
        onItemPress={onItemPress}
        isDialogOpen={isDialogOpen}
        buttonGroupOpen={buttonGroupOpen}
        onDialogToggle={onDialogToggle}
        onEditPress={this.handleEditPress}
        onItemCreate={onItemCreate}
        onRemovePress={onRemovePress}
        onItemRemove={onItemRemove}
        onExpandPress={onExpandPress}
        onMinimizePress={onMinimizePress}
        onPressOpenButtonGroup={onPressOpenButtonGroup}
      />
    );
  }
}

const styles = StyleSheet.create({
  openedFormView: {
    backgroundColor: material.bgColor,
    borderWidth: 1,
    borderColor: material.brColor,
    borderRadius: 10,
    margin: 2,
    padding: 5
  }
})
