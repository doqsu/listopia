import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
    Container,
    Content,
    StyleProvider,
    View,
    Button,
    Text,
    Icon,
    ActionSheet,
} from 'native-base';

import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';

import ToggleableListForm from './ToggleableListForm';
import ListTemp from './ListTemp';

export default class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonGroupOpen: false
        }
    }
    //EXPANDS ACTIONS
    handleExpandPress = () => {
        const { id, onExpandPress } = this.props;
        onExpandPress(id);
    };

    handleMinimizePress = () => {
        const { id, onMinimizePress } = this.props;

        onMinimizePress(id);
    };

    //DELETE ITEM
    handleRemovePress = () => {
        const { id, onRemovePress } = this.props;

        onRemovePress(id);
    };

    handlePressOpenButtonGroup = () => {
        const { id, onPressOpenButtonGroup } = this.props;

        onPressOpenButtonGroup(id);
    }

    //render button group

    renderButtonGroup() {
        if (this.props.buttonGroupOpen) {
            <View style={{ flexDirection: 'column' }}>
                <Button transparent small primary onPress={this.handlePressOpenButtonGroup}>
                    <Icon type='Entypo' name='dot-single' />
                </Button>
                <Button transparent small primary onPress={onEditPress}>
                    <Icon type='Entypo' name='edit' />
                </Button>
                <Button transparent small primary onPress={this.handleRemovePress}>
                    <Icon type='MaterialCommunityIcons' fontSize={50} name='delete' />
                </Button>
            </View>

        } else if (!this.props.buttonGroupOpen) {
            <View style={{ flexDirection: 'column' }}>
                <Button transparent small primary onPress={() => this.setState({ buttonGroupOpen: !this.state.buttonGroupOpen })}>
                    <Icon type='Entypo' name='dots-three-vertical' />
                </Button>
            </View>
        }
    }
    //EXPAND
    renderExpandButton() {
        const { isExpand } = this.props;

        if (isExpand) {
            return (
                <View style={{ flexDirection: 'column' }}>
                    {this.props.buttonGroupOpen ?
                        <View style={{ flexDirection: 'column' }}>
                            <Button transparent small primary onPress={this.handlePressOpenButtonGroup}>
                                <Icon type='Entypo' name='dot-single' />
                            </Button>
                            <Button transparent small primary onPress={this.props.onEditPress}>
                                <Icon type='Entypo' name='edit' />
                            </Button>
                            <Button transparent small primary onPress={() => {
                                            ActionSheet.show(
                                                {
                                                  options: [
                                                      { text: 'Delete', icon: 'checkmark', iconColor: '#26242F'},
                                                      { text: 'Cancel', icon: 'close', iconColor: '#26242F'},
                                                  ],
                                                  title: "Are you sure?"
                                                },
                                                buttonIndex => {
                                                    buttonIndex == 0 ? this.handleRemovePress() : ActionSheet.hide()
                                                }
                                              )
                            }}>
                                <Icon type='MaterialCommunityIcons' size={50} name='delete' />
                            </Button>
                        </View> :
                        <View style={{ flexDirection: 'column' }}>
                            <Button transparent small primary onPress={this.handlePressOpenButtonGroup}>
                                <Icon type='Entypo' name='dots-three-vertical' />
                            </Button>
                        </View>
                    }
                </View>

            );
        }

        return;
    }

    render() {
        const {
            isExpand,
            title,
            onItemPress,
            onEditPress,
            positions,
            onItemCreate,
            onItemRemove,
            id
        } = this.props;

        if (isExpand) {
            return (
                <StyleProvider style={getTheme(material)}>
                    <View style={styles.mainListContainer}>
                        <View style={styles.upsideRow}>
                            <View style={styles.leftColumn}>
                                <TouchableOpacity onPress={this.handleMinimizePress}>
                                    <View style={styles.titleContainer}>
                                        <Text style={styles.listTitle}>{title}</Text>
                                    </View>
                                </TouchableOpacity>


                                <View style={styles.positionsContainer}>
                                    <ListTemp
                                        listId={id}
                                        isForPositions={true}
                                        data={positions}
                                        onItemPress={onItemPress}
                                        onFormSubmit={this.handleFormSubmit}
                                        onRemovePress={this.handleRemovePress}
                                        onItemRemove={onItemRemove}
                                        onStartPress={this.toggleList}
                                        onStopPress={this.toggleList}
                                        onExpandPress={this.toggleExpand}
                                        onMinimizePress={this.toggleExpand} />
                                </View>

                            </View>
                            <View
                                style={{
                                    justifyContent: 'flex-start'
                                }}>
                                <View style={styles.buttonGroup}>
                                    <View>
                                        {this.renderExpandButton()}
                                    </View>
                                </View>
                            </View>

                        </View>

                        <View style={styles.downsideButtonGroup}>

                            <ToggleableListForm
                                isForList={true}
                                onItemCreate={onItemCreate}
                                positions={positions}
                                listId={id}
                                title={title} />

                        </View>

                    </View>
                </StyleProvider>

            );
        } else {
            return (
                <StyleProvider style={getTheme(material)}>

                    <View style={styles.mainListContainer}>
                        <View style={styles.upsideRow}>
                            <View style={styles.leftColumn}>
                                <TouchableOpacity onPress={this.handleExpandPress}>
                                    <View style={styles.titleContainer}>
                                        <Text style={styles.listTitle}>{title}</Text>
                                    </View>
                                </TouchableOpacity>

                            </View>
                            <View style={styles.buttonGroup}>
                                <View>
                                    {this.renderExpandButton()}
                                </View>

                            </View>
                        </View>
                    </View>
                </StyleProvider>

            );
        }

    }
}

const styles = StyleSheet.create({
    mainListContainer: {
        flexDirection: 'column',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: material.brColor,
        backgroundColor: material.listBgColor,
        margin: 2,
        padding: 10

    },
    upsideRow: {
        flexDirection: 'row'
    },
    leftColumn: {
        flex: 1,
        flexDirection: 'column'
    },
    titleContainer: {
        width: '100%',
        padding: 14,
    },
    listTitle: {
        color: material.titleColor,
        fontWeight: '500',
        fontSize: 17
    },
    buttonGroup: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        margin: 4,
        borderRadius: 10,
        //borderWidth: 1,
        borderColor: material.brColor

    },
    downsideListContainer: {},
    positionsContainer: {
        width: '100%'
    },
    downsideButtonGroup: {
        alignItems: 'center',
        borderRadius: 5,
        margin: 4
    }
});