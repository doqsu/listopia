import React from 'react';
import { StyleSheet } from 'react-native';
import {
    Container,
    Content,
    StyleProvider,
    View,
    Header,
    Form,
    Input,
    Item,
    Button,
    Label,
    Text,
    Icon,
    H3,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import material from '../native-base-theme/variables/material';
import getTheme from '../native-base-theme/components';

export default class ListForm extends React.Component {
    constructor(props) {
        super(props);
        const { id, title } = props;
        this.state = {
            title: id
                ? title
                : ''
        };
    }

    handleTitleChange = title => {
        this.setState({ title });
    };

    handleSubmit = () => {
        const { onFormSubmit, id } = this.props;
        const { title } = this.state;

        onFormSubmit({ id, title });
    };

    render() {
        const { id, onFormClose, textInputLabel, isForList } = this.props;
        const { title } = this.state;

        const submitText = id
            ? 'Update'
            : 'Create';

        return (
            <StyleProvider style={getTheme(material)}>
                <View style={styles.listFormContainer}>
                    <Form>
                        <Grid>
                            <Row>
                                <View style={styles.textInputContainer}>
                                    <Item regular dark>
                                        <Input
                                            placeholder={isForList ? 'Item name' : 'List name'}
                                            style={styles.textInput}
                                            underlineColorAndroid="transparent"
                                            underlineColor="transparent"
                                            label={textInputLabel}
                                            selectionColor="#123123"

                                            value={title}
                                            autoCorrect={false}
                                            autoCapitalize="none"
                                            onChangeText={this.handleTitleChange} />
                                    </Item>
                                </View>

                                <View style={styles.buttonGroup}>
                                    <View style={styles.button}>
                                        <Button
                                            transparent
                                            primary
                                            small
                                            onPress={this.handleSubmit}>
                                            <Icon type='MaterialCommunityIcons' name='check' />
                                        </Button>
                                    </View>

                                    <View style={styles.button}>
                                        <Button
                                            transparent
                                            small
                                            primary
                                            onPress={onFormClose}>
                                            <Icon type='MaterialCommunityIcons' name='close' />
                                        </Button>
                                    </View>
                                </View>
                            </Row>
                        </Grid>

                    </Form>
                </View>
            </StyleProvider>

        );
    }
}

const styles = StyleSheet.create({
    listFormContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 10,
        height: 40,
    },
    textInputContainer: {
        width: '70%',
    },
    textInput: {
        backgroundColor: material.inputBg,
        borderWidth: 1,
        borderColor:  material.brColor,
        flex: 1,
        height: 40,
        borderRadius: 5,

    },
    buttonGroup: {
        flexDirection: 'row',
        alignItems: 'center',

    }
});
