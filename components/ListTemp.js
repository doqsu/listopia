import React from 'react';
import {StyleSheet, View, FlatList} from 'react-native';

import EditableList from './EditableList';
import ItemComponent from './ItemComponent';

export default class ListTemp extends React.Component {
    
    _keyExtractor = (item, index) => item.id;
    _renderList = ({item}) => (<EditableList
        title={item.title}
        positions={item.positions}
        isExpand={item.isExpand}
        id={item.id}
        buttonGroupOpen={item.buttonGroupOpen}
        onItemPress={this.props.onItemPress}
        onFormSubmit={this.props.onFormSubmit}
        onItemCreate={this.props.onItemCreate}
        onItemRemove={this.props.onItemRemove}
        onRemovePress={this.props.onRemovePress}
        onStartPress={this.props.onStartPress}
        onStopPress={this.props.onStopPress}
        onExpandPress={this.props.onExpandPress}
        onMinimizePress={this.props.onMinimizePress}
        onDialogToggle={this.props.onDialogToggle}
        onPressOpenButtonGroup={this.props.onPressOpenButtonGroup}
        />);

    _renderItems = ({item}) => (<ItemComponent
        listId={this.props.listId}
        id={item.id}
        title={item.title}
        isItemCompleted={item.isItemCompleted}
        isItemPressed={item.isItemPressed}
        onItemPress={this.props.onItemPress}
        onFormSubmit={this.props.onFormSubmit}
        onRemovePress={this.props.onRemovePress}
        onItemRemove={this.props.onItemRemove}
        onStartPress={this.props.onStartPress}
        onStopPress={this.props.onStopPress}
        onExpandPress={this.props.onExpandPress}
        onMinimizePress={this.props.onMinimizePress}/>);

    render() {
        const {data, isForPositions} = this.props;

        return (
            <View>
                <FlatList
                    data={data}
                    renderItem={isForPositions
                    ? this._renderItems
                    : this._renderList}
                    keyExtractor={this._keyExtractor}/>
            </View>
        )

    }
}