import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { StyleProvider, Text, View, Button, Icon } from 'native-base';
import material from '../native-base-theme/variables/material';
import getTheme from '../native-base-theme/components';

export default class ItemComponent extends React.Component {
  handleItemRemove = () => {
    const { onItemRemove, id, listId } = this.props;
    onItemRemove(listId, id)
  }
  render() {
    const {
      title,
    } = this.props;
      return (
        <StyleProvider style={getTheme(material)}>
          <View>
            <TouchableOpacity
              style={styles.itemContainer}
              onPress={this.handleItemRemove}>
              <Text style={styles.itemText}>{title}</Text>
            </TouchableOpacity>
          </View>
        </StyleProvider>
      );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    padding: 6,
    marginLeft: 8,
    borderRadius: 10,
    flexDirection: 'row',
  }
});
