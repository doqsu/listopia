import React from 'react';
import { StyleSheet } from 'react-native';

import {
  Container,
  Content,
  StyleProvider,
  View,
  Header,
  Form,
  Input,
  Item,
  Button,
  Label,
  Text,
  Icon,
} from 'native-base';

import ListForm from './ListForm';



export default class ToggleableListForm extends React.Component {
  state = {
    isOpen: false,
  };

  handleFormOpen = () => {
    this.setState({ isOpen: true });
  };

  handleFormClose = () => {
    this.setState({ isOpen: false });
  };

  handleFormSubmit = list => { //{id, title}
    const { onFormSubmit, isForList, listId, onItemCreate } = this.props;
    if (isForList) { //true
      onItemCreate(list.title, listId); //title, listId
      this.setState({ isOpen: false });
    }
    else {
      onFormSubmit(list);
      this.setState({ isOpen: false });
    }
  };

  render() {
    const { isOpen } = this.state;
    const { isForList } = this.props;


    return (
      <View style={styles.toggleableListForm}>
        {isOpen ? (
          <ListForm
            textInputLabel={isForList ? 'Item' : 'Title'}
            onFormSubmit={this.handleFormSubmit}
            onFormClose={this.handleFormClose}
            isForList={isForList}
          />
        ) : (
            isForList ?
              <Button iconLeft transparent small primary onPress={this.handleFormOpen}>
                <Icon type='MaterialIcons' fontSize={50} name='add-circle-outline'/>
              </Button> :
              <Button iconLeft rounded primary onPress={this.handleFormOpen}>
                <Icon type='MaterialIcons' fontSize={50} name='add'/>
                <Text>Create new list</Text>
              </Button>
          )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  toggleableListForm: {
    alignItems: 'center',
    flex: 1,
    alignSelf: 'center',
    padding: 6,
  },

});
