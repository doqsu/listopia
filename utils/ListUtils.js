import uuidv4 from 'uuid/v4';

export const newList = (attrs = {}, email) => {
  const list = {
    title: attrs.title || 'List',
    id: uuidv4(),
    positions: [],
    isExpand: false,
    isDialogOpen: false,
    buttonGroupOpen: false,
    email: email
  };

  return list;
};

export const newItem = ( title ) => {
  const listItem = {
    title: title || 'item',
    isItemPressed: false,
    isItemCompleted: false,
    id: uuidv4(),
    isDialogOpen: false,
  }

  return listItem;
}